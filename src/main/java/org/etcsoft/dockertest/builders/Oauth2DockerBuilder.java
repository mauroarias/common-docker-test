package org.etcsoft.dockertest.builders;

import lombok.SneakyThrows;
import org.etcsoft.dockertest.docker.MysqlDefaultDocker;
import org.etcsoft.dockertest.docker.MysqlDocker;
import org.etcsoft.dockertest.docker.SpringDockerApp;

import static org.etcsoft.dockertest.docker.DockerConstants.*;

public final class Oauth2DockerBuilder {
    private String mysqlImageName = DOCKER_MYSQL_IMAGE;
    private String mysqlImageTag = DOCKER_MYSQL_IMAGE_TAG;
    private String oauth2ServerImageName = DOCKER_OAUTH2_SERVER_IMAGE;
    private String oauth2ServerImageTag = DOCKER_SPRING_SERVER_TAG;
    private String authServerPort = DOCKER_OAUTH2_PORT;
    private String hostname;
    private boolean autoPull = false;

    public Oauth2DockerBuilder mysqlImageName(String mysqlImageName) {
        this.mysqlImageName = mysqlImageName;
        return this;
    }

    public Oauth2DockerBuilder mysqlImageTag(String mysqlImageTag) {
        this.mysqlImageTag = mysqlImageTag;
        return this;
    }

    public Oauth2DockerBuilder hostname(String hostname) {
        this.hostname = hostname;
        return this;
    }

    public Oauth2DockerBuilder autoPull(boolean autoPull) {
        this.autoPull = autoPull;
        return this;
    }

    public Oauth2DockerBuilder oauth2ServerImageName(String oauth2ServerImageName) {
        this.oauth2ServerImageName = oauth2ServerImageName;
        return this;
    }

    @SneakyThrows
    public SpringDockerApp build() {
        MysqlDocker dockerMysql = new MysqlDefaultDocker(mysqlImageName,
                mysqlImageTag,
                autoPull,
                hostname,
                null,
                null);
        try {
            dockerMysql.loadSchema(SCHEMA);
            dockerMysql.loadSchema(USERS);
            SpringDockerApp authserver = new SpringAppDockerBuilder()
                    .imageName(oauth2ServerImageName)
                    .imageTag(oauth2ServerImageTag)
                    .addExposedPort(authServerPort)
                    .addContainerLink(dockerMysql.getContainerId(), DOCKER_OAUTH2_CONTAINER_ALIAS)
                    .addParentContainer(DOCKER_OAUTH2_MYSQL_NAME, dockerMysql)
                    .build();
            return authserver;
        } catch (Exception ex) {
            if(dockerMysql != null) {
                dockerMysql.close();
            }
            throw ex;
        }
    }
}
