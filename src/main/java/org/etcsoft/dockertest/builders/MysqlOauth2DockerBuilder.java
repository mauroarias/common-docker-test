package org.etcsoft.dockertest.builders;

import lombok.SneakyThrows;
import org.apache.commons.lang.StringUtils;
import org.etcsoft.dockertest.DockerErrorCodes;
import org.etcsoft.dockertest.DockerIllegalException;
import org.etcsoft.dockertest.docker.*;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static java.lang.String.format;
import static org.etcsoft.dockertest.docker.DockerConstants.SCHEMA;
import static org.etcsoft.dockertest.docker.DockerConstants.USERS;

public final class MysqlOauth2DockerBuilder {
    private String imageName = DockerConstants.DOCKER_MYSQL_IMAGE;
    private String imageTag = DockerConstants.DOCKER_MYSQL_IMAGE_TAG;
    private String hostname;
    private final Set<String> containerLinks = new HashSet<>();
    private final Map<String, DockerContainer> parentContainers = new HashMap<>();
    private boolean autoPull = false;

    public MysqlOauth2DockerBuilder imageName(String mysqlImageName) {
        this.imageName = mysqlImageName;
        return this;
    }

    public MysqlOauth2DockerBuilder imageTag(String mysqlImageTag) {
        this.imageTag = mysqlImageTag;
        return this;
    }

    public MysqlOauth2DockerBuilder hostname(String hostname) {
        this.hostname = hostname;
        return this;
    }

    public MysqlOauth2DockerBuilder autoPull(boolean autoPull) {
        this.autoPull = autoPull;
        return this;
    }

    public MysqlOauth2DockerBuilder addContainerLink(String containerId, String alias) {
        stringArgumentValidator(containerId, "containerId cannot be empty or null");
        stringArgumentValidator(containerId, "alias cannot be empty or null");
        this.containerLinks.add(format("%s:%s", containerId, alias));
        return this;
    }

    public MysqlOauth2DockerBuilder addParentContainer(String name, DockerContainer container) {
        this.parentContainers.put(name, container);
        return this;
    }

    @SneakyThrows
    public MysqlDocker build() {
        MysqlDocker docker = new MysqlDefaultDocker(imageName,
                imageTag,
                autoPull,
                hostname,
                containerLinks,
                parentContainers);
        try {
            docker.loadSchema(SCHEMA);
            docker.loadSchema(USERS);
        } catch (Exception ex) {
            if(docker != null) {
                docker.close();
            }
            throw ex;
        }
        return docker;
    }

    private void stringArgumentValidator(String arg, String argName) {
        if(StringUtils.isBlank(arg)) {
            throw new DockerIllegalException(DockerErrorCodes.WRONG_VALIDATION,
                    format("%s cannot be empty or null", argName));
        }
    }
}
