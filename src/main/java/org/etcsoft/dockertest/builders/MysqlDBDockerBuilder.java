package org.etcsoft.dockertest.builders;

import org.apache.commons.lang.StringUtils;
import org.etcsoft.dockertest.DockerErrorCodes;
import org.etcsoft.dockertest.DockerIllegalException;
import org.etcsoft.dockertest.docker.*;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static java.lang.String.format;

public final class MysqlDBDockerBuilder {
    private String mysqlImageName = DockerConstants.DOCKER_MYSQL_IMAGE;
    private String mysqlImageTag = DockerConstants.DOCKER_MYSQL_IMAGE_TAG;
    private String hostname;
    private final Set<String> containerLinks = new HashSet<>();
    private final Map<String, DockerContainer> parentContainers = new HashMap<>();
    private boolean autoPull = false;

    public MysqlDBDockerBuilder mysqlImageName(String mysqlImageName) {
        this.mysqlImageName = mysqlImageName;
        return this;
    }

    public MysqlDBDockerBuilder mysqlImageTag(String mysqlImageTag) {
        this.mysqlImageTag = mysqlImageTag;
        return this;
    }

    public MysqlDBDockerBuilder hostname(String hostname) {
        this.hostname = hostname;
        return this;
    }

    public MysqlDBDockerBuilder autoPull(boolean autoPull) {
        this.autoPull = autoPull;
        return this;
    }

    public MysqlDBDockerBuilder addContainerLink(String containerId, String alias) {
        stringArgumentValidator(containerId, "containerId cannot be empty or null");
        stringArgumentValidator(containerId, "alias cannot be empty or null");
        this.containerLinks.add(format("%s:%s", containerId, alias));
        return this;
    }

    public MysqlDBDockerBuilder addParentContainer(String name, DockerContainer container) {
        this.parentContainers.put(name, container);
        return this;
    }

    public MysqlDocker build() {
        return new MysqlDefaultDocker(mysqlImageName,
                mysqlImageTag,
                autoPull,
                hostname,
                containerLinks,
                parentContainers);
    }

    private void stringArgumentValidator(String arg, String argName) {
        if(StringUtils.isBlank(arg)) {
            throw new DockerIllegalException(DockerErrorCodes.WRONG_VALIDATION,
                    format("%s cannot be empty or null", argName));
        }
    }
}
