package org.etcsoft.dockertest.builders;

import org.apache.commons.lang.StringUtils;
import org.etcsoft.dockertest.DockerErrorCodes;
import org.etcsoft.dockertest.DockerIllegalException;
import org.etcsoft.dockertest.docker.DockerConstants;
import org.etcsoft.dockertest.docker.DockerContainer;
import org.etcsoft.dockertest.docker.SpringDefaultDockerApp;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static java.lang.String.format;

public final class SpringAppDockerBuilder {
    private String imageName;
    private String imageTag = DockerConstants.DOCKER_SPRING_SERVER_TAG;
    private String hostname;
    private final Set<String> environmentVars = new HashSet<>();
    private final Set<String> containerLinks = new HashSet<>();
    private final Set<String> exposedPorts = new HashSet<>();
    private final Map<String, DockerContainer> parentContainers = new HashMap<>();
    private boolean autoPull = false;

    public SpringAppDockerBuilder imageName(String imageName) {
        this.imageName = imageName;
        return this;
    }

    public SpringAppDockerBuilder imageTag(String imageTag) {
        this.imageTag = imageTag;
        return this;
    }

    public SpringAppDockerBuilder hostname(String hostname) {
        this.hostname = hostname;
        return this;
    }

    public SpringAppDockerBuilder autoPull(boolean autoPull) {
        this.autoPull = autoPull;
        return this;
    }

    public SpringAppDockerBuilder addContainerLink(String containerId, String alias) {
        stringArgumentValidator(containerId, "containerId cannot be empty or null");
        stringArgumentValidator(containerId, "alias cannot be empty or null");
        this.containerLinks.add(format("%s:%s", containerId, alias));
        return this;
    }

    public SpringAppDockerBuilder addExposedPort(String port) {
        exposedPorts.add(port);
        return this;
    }

    public SpringAppDockerBuilder addEnvironmentVar(String var) {
        environmentVars.add(var);
        return this;
    }

    public SpringAppDockerBuilder addParentContainer(String name, DockerContainer container) {
        this.parentContainers.put(name, container);
        return this;
    }

    public SpringDefaultDockerApp build() {
        return new SpringDefaultDockerApp(imageName,
                imageTag,
                autoPull,
                exposedPorts,
                environmentVars,
                hostname,
                containerLinks,
                parentContainers);
    }

    private void stringArgumentValidator(String arg, String argName) {
        if(StringUtils.isBlank(arg)) {
            throw new DockerIllegalException(DockerErrorCodes.WRONG_VALIDATION,
                    format("%s cannot be empty or null", argName));
        }
    }
}
