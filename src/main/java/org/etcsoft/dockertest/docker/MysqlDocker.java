package org.etcsoft.dockertest.docker;

import com.zaxxer.hikari.HikariDataSource;

import java.util.List;

public interface MysqlDocker extends DockerContainer {

    HikariDataSource getDatasource();
    void loadSchema(List<String> statements);
}
