package org.etcsoft.dockertest.docker;

import com.spotify.docker.client.DefaultDockerClient;
import com.spotify.docker.client.DockerClient;

import java.util.Map;
import java.util.Set;

import static org.etcsoft.dockertest.docker.DockerConstants.DOCKER_SPRING_WAITNG_LOOP;

public final class SpringDefaultDockerApp extends DockerAbstractContainer implements SpringDockerApp {

    public SpringDefaultDockerApp(String imageName,
                                  String imageTag,
                                  boolean autoPull,
                                  Set<String> exposedPorts,
                                  Set<String> environmentVars,
                                  String hostname,
                                  Set<String> containerLinks,
                                  Map<String, DockerContainer> parentContainer) {
        this(imageName,
                imageTag,
                autoPull,
                exposedPorts,
                environmentVars,
                null,
                DOCKER_SPRING_WAITNG_LOOP,
                hostname,
                containerLinks,
                parentContainer);
    }

    SpringDefaultDockerApp(String imageName,
                           String imageTag,
                           boolean autoPull,
                           Set<String> exposedPorts,
                           Set<String> environmentVars,
                           DockerClient dockerClient,
                           int secWaitingLoop,
                           String hostname,
                           Set<String> containerLinks,
                           Map<String, DockerContainer> parentContainer) {
        super(imageName,
                imageTag,
                autoPull,
                environmentVars,
                secWaitingLoop,
                DockerConstants.DOCKER_SPRING_WAITING_START_APP_MESSAGE,
                dockerClient == null ? new DefaultDockerClient(DockerConstants.DOCKER_URI) : dockerClient,
                exposedPorts,
                hostname,
                containerLinks,
                parentContainer
        );
    }

    public String getExposedRandomPort(String port) {
        return getRandomPort(port);
    }
}
