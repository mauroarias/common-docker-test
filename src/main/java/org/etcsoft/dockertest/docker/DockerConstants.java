package org.etcsoft.dockertest.docker;

import java.util.Arrays;
import java.util.List;

public class DockerConstants {
    public final static String DOCKER_URI = "unix:///var/run/docker.sock";
    public final static String DOCKER_NETWORK_DRIVER = "bridge";
    public final static int DOCKER_WAITING_LOOP_DURATION = 1000; //in miliseconds
    public final static boolean DOCKER_CHECK_DUPLICATE_NETWORK = true;
    public final static boolean AUTO_PULL = false;

    //mysql default docker image and config
    public final static String DOCKER_MYSQL_IMAGE = "mauroarias/mysql-oraclelinux7";
    public final static String DOCKER_MYSQL_IMAGE_TAG = "5.6.34";
    public final static String DOCKER_MYSQL_WAITING_START_MESSAGE = "MySQL init process done. Ready for start up";
    public final static String DOCKER_MYSQL_USER = "root";
    public final static String DOCKER_MYSQL_PASSWD = "admin";
    public final static String DOCKER_MYSQL_HOST = "localhost";
    public final static int DOCKER_MYSQL_WAITNG_LOOP = 60;

    //spring defaul docker config
    public final static String DOCKER_SPRING_WAITING_START_APP_MESSAGE = "Started MainApplication in ";
    public final static String DOCKER_SPRING_STARTING_FAIL = "Application startup failed";
    public final static String DOCKER_SPRING_SERVER_TAG = "latest";
    public final static int DOCKER_SPRING_WAITNG_LOOP = 60;

    //Mysql Oauth2 container
    public final static String DOCKER_OAUTH2_SERVER_IMAGE = "mauroarias/authserver";
    public final static String DOCKER_OAUTH2_CONTAINER_ALIAS = "oauth2";
    public final static String DOCKER_OAUTH2_PORT = "8081";
    public final static String DOCKER_OAUTH2_MYSQL_NAME = "oauth2Mysql";

    public final static List<String> SCHEMA = Arrays.asList(
            "CREATE DATABASE oauth2;",

            "use oauth2;",

            "DROP TABLE IF EXISTS `ClientDetails`;",

            "CREATE TABLE `ClientDetails` (" +
                    "   `appId` varchar(256) NOT NULL," +
                    "   `resourceIds` varchar(256) DEFAULT NULL," +
                    "   `appSecret` varchar(256) DEFAULT NULL," +
                    "   `scope` varchar(256) DEFAULT NULL," +
                    "   `grantTypes` varchar(256) DEFAULT NULL," +
                    "   `redirectUrl` varchar(256) DEFAULT NULL," +
                    "   `authorities` varchar(256) DEFAULT NULL," +
                    "   `access_token_validity` int(11) DEFAULT NULL," +
                    "   `refresh_token_validity` int(11) DEFAULT NULL," +
                    "   `additionalInformation` varchar(4096) DEFAULT NULL," +
                    "PRIMARY KEY (`appId`)" +
                    ") ENGINE=InnoDB DEFAULT CHARSET=latin1;",

            "DROP TABLE IF EXISTS `authorities`;",

            "CREATE TABLE `authorities` (" +
                    "   `username` varchar(50) NOT NULL," +
                    "   `authority` varchar(50) NOT NULL," +
                    "UNIQUE KEY `ix_auth_username` (`username`,`authority`)" +
                    ") ENGINE=InnoDB DEFAULT CHARSET=latin1;",

            "DROP TABLE IF EXISTS `oauth_access_token`;",

            "CREATE TABLE `oauth_access_token` (" +
                    "   `token_id` varchar(256) DEFAULT NULL," +
                    "   `token` blob," +
                    "   `authentication_id` varchar(256) DEFAULT NULL," +
                    "   `user_name` varchar(256) DEFAULT NULL," +
                    "   `client_id` varchar(256) DEFAULT NULL," +
                    "   `authentication` blob," +
                    "   `refresh_token` varchar(256) DEFAULT NULL" +
                    ") ENGINE=InnoDB DEFAULT CHARSET=latin1;",

            "DROP TABLE IF EXISTS `oauth_client_details`;",

            "CREATE TABLE `oauth_client_details` (" +
                    "   `client_id` varchar(256) NOT NULL," +
                    "   `resource_ids` varchar(256) DEFAULT NULL," +
                    "   `client_secret` varchar(256) DEFAULT NULL," +
                    "   `scope` varchar(256) DEFAULT NULL," +
                    "   `authorized_grant_types` varchar(256) DEFAULT NULL," +
                    "   `web_server_redirect_uri` varchar(256) DEFAULT NULL," +
                    "   `authorities` varchar(256) DEFAULT NULL," +
                    "   `access_token_validity` int(11) DEFAULT NULL," +
                    "   `refresh_token_validity` int(11) DEFAULT NULL," +
                    "   `additional_information` varchar(4096) DEFAULT NULL," +
                    "   `autoapprove` varchar(256) DEFAULT NULL," +
                    "PRIMARY KEY (`client_id`)" +
                    ") ENGINE=InnoDB DEFAULT CHARSET=latin1;",

            "DROP TABLE IF EXISTS `oauth_code`;",

            "CREATE TABLE `oauth_code` (" +
                    "   `code` varchar(256) DEFAULT NULL," +
                    "   `authentication` blob" +
                    ") ENGINE=InnoDB DEFAULT CHARSET=latin1;",

            "DROP TABLE IF EXISTS `oauth_refresh_token`;",

            "CREATE TABLE `oauth_refresh_token` (" +
                    "   `token_id` varchar(256) DEFAULT NULL," +
                    "   `token` blob," +
                    "   `authentication` blob" +
                    ") ENGINE=InnoDB DEFAULT CHARSET=latin1;",

            "DROP TABLE IF EXISTS `users`;",

            "CREATE TABLE `users` (" +
                    "   `username` varchar(100) NOT NULL," +
                    "   `password` varchar(100) NOT NULL," +
                    "   `enabled` tinyint(1) NOT NULL," +
                    "PRIMARY KEY (`username`)" +
                    ") ENGINE=InnoDB DEFAULT CHARSET=latin1;"
    );

    public final static List<String> USERS = Arrays.asList(
            "use oauth2;",
            "INSERT INTO users (`username`, `password`, `enabled`) " +
                    "   VALUES ('dave', '$2a$10$4X1DC8WqtutUWH.CwrJREOt1Ofohd9IEMZ3G2Hkl5hVhU/YaWaUZu', 1);",
            "INSERT INTO oauth_client_details (`client_id`, `resource_ids`, `client_secret`, `scope`, `authorized_grant_types`, `authorities`, `additional_information`)" +
                    "   VALUES ('my-client-with-secret', 'oauth2-resource', '$2a$10$iOWyz5EVXAePc0uBqgfSM.Zc2sdwqFtYmyY1h8/PpVbwsidzB87Ym', 'read', 'client_credentials,password', 'ROLE_CLIENT', '{}');",
            "INSERT INTO oauth_client_details (`client_id`, `resource_ids`, `scope`, `authorized_grant_types`, `authorities`, `access_token_validity`, `additional_information`)" +
                    "   VALUES ('my-trusted-client', 'oauth2-resource', 'read,write,trust', 'password,authorization_code,refresh_token,implicit', 'ROLE_CLIENT,ROLE_TRUSTED_CLIENT', 60, '{}');"
    );


}
