package org.etcsoft.dockertest.docker;

import com.spotify.docker.client.DefaultDockerClient;
import com.spotify.docker.client.DockerClient;
import com.zaxxer.hikari.HikariDataSource;
import lombok.SneakyThrows;
import org.etcsoft.dockertest.DockerErrorCodes;
import org.etcsoft.dockertest.DockerIllegalException;

import java.sql.Connection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static java.lang.String.format;
import static org.etcsoft.dockertest.docker.DockerConstants.DOCKER_MYSQL_WAITNG_LOOP;

public final class MysqlDefaultDocker extends DockerAbstractContainer implements MysqlDocker {
    private final static String mysqlPort = "3306";
    private HikariDataSource hikariDatasource = null;

    public MysqlDefaultDocker(String imageName,
                              String imageTag,
                              boolean autoPull,
                              String hostname,
                              Set<String> containerLinks,
                              Map<String, DockerContainer> parentContainers) {
        this(imageName,
                imageTag,
                autoPull,
                null,
                null,
                DOCKER_MYSQL_WAITNG_LOOP,
                hostname,
                containerLinks,
                parentContainers);
    }

    MysqlDefaultDocker(String imageName,
                       String imageTag,
                       boolean autoPull,
                       DockerClient dockerClient,
                       HikariDataSource hikariDatasource,
                       int secWaitingLoop,
                       String hostname,
                       Set<String> containerLinks,
                       Map<String, DockerContainer> parentContainers) {
        super(imageName,
                imageTag,
                autoPull,
                new HashSet<String>() {{
                        add(format("MYSQL_ROOT_PASSWORD=%s", DockerConstants.DOCKER_MYSQL_PASSWD));
                        add("MYSQL_ROOT_HOST=%");
                }},
                secWaitingLoop,
                DockerConstants.DOCKER_MYSQL_WAITING_START_MESSAGE,
                dockerClient == null ? new DefaultDockerClient(DockerConstants.DOCKER_URI) : dockerClient,
                new HashSet<String>() {{ add(mysqlPort); }},
                hostname,
                containerLinks,
                parentContainers
        );
        if(hikariDatasource != null) {
            this.hikariDatasource = hikariDatasource;
        } else {
            this.hikariDatasource = getDatasource();
        }
    }

    public HikariDataSource getDatasource() {
        if(hikariDatasource == null || hikariDatasource.isClosed()) {
            //starting Hikari datasource
            hikariDatasource = new HikariDataSource();
            hikariDatasource.setDriverClassName("com.mysql.jdbc.Driver");
            hikariDatasource.setJdbcUrl(format("jdbc:mysql://%s:%s",
                    DockerConstants.DOCKER_MYSQL_HOST,
                    getRandomPort(mysqlPort)));
            hikariDatasource.setUsername(DockerConstants.DOCKER_MYSQL_USER);
            hikariDatasource.setPassword(DockerConstants.DOCKER_MYSQL_PASSWD);
            hikariDatasource.setAutoCommit(false);
        }
        return hikariDatasource;
    }

    @Override
    public void close() throws Exception {
        //closing datasource
        if(hikariDatasource != null) {
            hikariDatasource.close();
        }
        closeContainer();
    }

    @SneakyThrows
    public void loadSchema(List<String> statements) {
        Class.forName("com.mysql.jdbc.Driver");
        try (Connection connection =
                     hikariDatasource.getConnection()) {
            try {
                for (String statement : statements) {
                    connection.prepareStatement(statement).execute();
                }
                connection.commit();
            } catch (Exception ex) {
                connection.rollback();
                throw ex;
            }
        } catch (Exception ex) {

            throw new DockerIllegalException(DockerErrorCodes.DATABASE_SCHEMA_ERROR,
                    format("Error loading schema, exception: %s!!!", ex.getMessage()));
        }
    }
}
