package org.etcsoft.dockertest.docker;

import java.util.Map;

public interface DockerContainer {
    void close() throws Exception;
    String getImageFullName();
    String getContainerId();
    boolean isClosed();
    Map<String, DockerContainer> getParentContainers();
}
