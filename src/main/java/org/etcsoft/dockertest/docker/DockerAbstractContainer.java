package org.etcsoft.dockertest.docker;

import com.spotify.docker.client.DockerClient;
import com.spotify.docker.client.LogStream;
import com.spotify.docker.client.exceptions.DockerException;
import com.spotify.docker.client.messages.ContainerConfig;
import com.spotify.docker.client.messages.HostConfig;
import com.spotify.docker.client.messages.PortBinding;
import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.commons.lang.StringUtils;
import org.etcsoft.dockertest.DockerErrorCodes;
import org.etcsoft.dockertest.DockerIllegalException;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static java.lang.String.format;
import static org.etcsoft.dockertest.docker.DockerConstants.DOCKER_SPRING_STARTING_FAIL;

abstract class DockerAbstractContainer implements AutoCloseable {
    private final String dockerWaitingMessage;
    private final String hostname;
    private final Set<String> containerLinks = new HashSet<>();
    private final Set<String> environmentsVars = new HashSet<>();
    private final Map<String, DockerContainer> parentContainers = new HashMap<>();
    private final int dockerWaitingLoopTime;

    protected final DockerClient dockerClient;
    @Getter
    protected final String imageFullName;
    @Getter
    protected String containerId;
    @Getter
    protected boolean closed = true;
    @Getter
    protected String containerName;
    protected final Set<String> exposedPorts = new HashSet<>();

    DockerAbstractContainer(String imageName,
                            String imageTag,
                            boolean autoPull,
                            Set<String> environmentsVars,
                            int dockerWaitingLoopTime,
                            String dockerWaitingMessage,
                            DockerClient dockerClient,
                            Set<String> exposedPorts,
                            String hostname,
                            Set<String> containerLinks,
                            Map<String, DockerContainer> parentContainers) {
        try {
            //init variables
            this.dockerWaitingMessage = dockerWaitingMessage;
            this.dockerWaitingLoopTime = dockerWaitingLoopTime;
            this.dockerClient = dockerClient;
            this.imageFullName = format("%s:%s", imageName, StringUtils.isBlank(imageTag) ? "latest" : imageTag);
            this.hostname = hostname;
            copyHashSet(environmentsVars, this.environmentsVars);
            copyHashSet(containerLinks, this.containerLinks);
            copyHashSet(exposedPorts, this.exposedPorts);
            if(parentContainers != null) {
                parentContainers.forEach((name, container) -> this.parentContainers.put(name, container));
            }
            //argument validator
            stringArgumentValidator(imageName, "Image name");
            stringArgumentValidator(dockerWaitingMessage, "Waiting message");
            if(dockerClient == null) {
                throw new DockerIllegalException(DockerErrorCodes.WRONG_VALIDATION, "Docker client cannot be null");
            }
            //pulling image
            if (autoPull) {
                dockerClient.pull(this.imageFullName);
            }
            //start and wait for container
            this.containerId = startAndWait4Image();
            closed = false;
        }
        catch (DockerException ex) {
            throw new DockerIllegalException(DockerErrorCodes.DOCKER_CLIENT_ERROR, ex.getMessage());
        }
        catch (InterruptedException ex) {
            throw new DockerIllegalException(DockerErrorCodes.DOCKER_INTERRUPTED, ex.getMessage());
        }
        catch (DockerIllegalException ex) {
            throw ex;
        }
        catch (Exception ex) {
            throw new DockerIllegalException(DockerErrorCodes.UNKNOWN_ERROR, ex.getMessage());
        }
    }

    public Set<String> getExposedPorts() {
        return new HashSet<>(this.exposedPorts);
    }

    public Map<String, DockerContainer> getParentContainers() {
        return new HashMap<>(this.parentContainers);
    }

    @Override
    @SneakyThrows
    public void close() throws Exception {
        for(DockerContainer container : this.parentContainers.values()) {
            container.close();
        }
        closeContainer();
    }

    protected String getRandomPort(String port) {
        try {
            final PortBinding portBinding = dockerClient
                    .inspectContainer(containerId)
                    .networkSettings()
                    .ports()
                    .get(format("%s/tcp", port)).stream().findFirst().get();

            if(portBinding == null || StringUtils.isBlank(portBinding.hostPort())) {
                return null;
            }

            return portBinding.hostPort();
        }
        catch (DockerException ex) {
            throw new DockerIllegalException(DockerErrorCodes.DOCKER_CLIENT_ERROR, ex.getMessage());
        }
        catch (InterruptedException ex) {
            throw new DockerIllegalException(DockerErrorCodes.DOCKER_INTERRUPTED, ex.getMessage());
        }
        catch (DockerIllegalException ex) {
            throw ex;
        }
        catch (Exception ex) {
            throw new DockerIllegalException(DockerErrorCodes.UNKNOWN_ERROR, ex.getMessage());
        }
    }

    protected void closeContainer() {
        try {
            //Stopping container & closing docker client
            if (dockerClient != null) {
                if (!StringUtils.isBlank(containerId)) {
                    dockerClient.stopContainer(containerId, 20);
                    dockerClient.removeContainer(containerId);
                }
                dockerClient.close();
                closed = true;
            }
        }
        catch (DockerException ex) {
            throw new DockerIllegalException(DockerErrorCodes.DOCKER_CLIENT_ERROR, ex.getMessage());
        }
        catch (InterruptedException ex) {
            throw new DockerIllegalException(DockerErrorCodes.DOCKER_INTERRUPTED, ex.getMessage());
        }
        catch (DockerIllegalException ex) {
            throw ex;
        }
        catch (Exception ex) {
            throw new DockerIllegalException(DockerErrorCodes.UNKNOWN_ERROR, ex.getMessage());
        }
    }

    protected void stringArgumentValidator(String arg, String argName) {
        if(StringUtils.isBlank(arg)) {
            throw new DockerIllegalException(DockerErrorCodes.WRONG_VALIDATION,
                    format("%s cannot be empty or null", argName));
        }
    }

    protected void nullArgumentValidator(Object object, String objectName) {
        if(object == null) {
            throw new DockerIllegalException(DockerErrorCodes.WRONG_VALIDATION,
                    format("%s cannot be null", objectName));
        }
    }

    private void copyHashSet(Set<String> source, Set<String> dest) {
        if(source != null) {
            source.forEach(item -> dest.add(item));
        }
    }

    private Map<String, List<PortBinding>> buildPortBuilding() {
        final Map<String, List<PortBinding>> portBindings = new HashMap<>();
        if(exposedPorts != null) {
            for (String port : exposedPorts) {
                List<PortBinding> hostPorts = new ArrayList<>();
                hostPorts.add(PortBinding.randomPort("0.0.0.0"));
                portBindings.put(format("%s/tcp", port), hostPorts);
            }
        }
        return portBindings;
    }

    private HostConfig buildHostConfig() {
        return HostConfig
                .builder()
                .links(containerLinks.toArray(new String[containerLinks.size()]))
                .portBindings(buildPortBuilding())
                .publishAllPorts(true)
                .build();
    }

    private String startAndWait4Image() throws DockerException, InterruptedException, IOException {
        final String containerId = prepareCreateContainer();
        dockerClient.startContainer(containerId);
        if(waitContainerUp(containerId)) {
            return containerId;
        }
        throw new DockerIllegalException(DockerErrorCodes.DOCKER_STARTING_TIMEOUT,
                "Timeout expired during starting container!!!");
    }

    private Set<String> buildExposedPorts() {
        Set<String> exposedPorts = new HashSet<>();
        if(this.exposedPorts != null) {
            exposedPorts = this.exposedPorts.stream().map(port -> format("%s/tcp", port)).collect(Collectors.toSet());
        }
        return exposedPorts;
    }

    private String prepareCreateContainer() throws DockerException, InterruptedException {
        //prepare container config
        final ContainerConfig containerConfig = ContainerConfig
                .builder()
                .hostConfig(buildHostConfig())
                .attachStdin(true)
                .hostname(hostname)
                .image(imageFullName)
                .env(environmentsVars.toArray(new String[environmentsVars.size()]))
                .exposedPorts(buildExposedPorts())
                .build();
        return dockerClient.createContainer(containerConfig).id();
    }

    private boolean waitContainerUp(String containerId) throws DockerException, InterruptedException, IOException {
        //waiting for container
        int waitCounter = dockerWaitingLoopTime;
        while (waitCounter-- != 0) {
            final LogStream dockerLog = dockerClient.logs(
                    containerId,
                    DockerClient.LogsParam.stderr(),
                    DockerClient.LogsParam.stdout());
            try {
                final String logStream = dockerLog.readFully();
                if (logStream.contains(dockerWaitingMessage)) {
                    Thread.sleep(DockerConstants.DOCKER_WAITING_LOOP_DURATION);
                    return true;
                }
                if (logStream.contains(DOCKER_SPRING_STARTING_FAIL)) {
                    dockerClient.removeContainer(containerId);
                    throw new DockerIllegalException(DockerErrorCodes.DOCKER_STARTING_ERROR, logStream);
                }
                Thread.sleep(DockerConstants.DOCKER_WAITING_LOOP_DURATION);
            } finally {
                if (dockerLog != null) {
                    dockerLog.close();
                }
            }
        }
        return false;
    }
}
