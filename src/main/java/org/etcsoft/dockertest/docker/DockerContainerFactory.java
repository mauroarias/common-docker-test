package org.etcsoft.dockertest.docker;

import org.etcsoft.dockertest.builders.MysqlDBDockerBuilder;
import org.etcsoft.dockertest.builders.MysqlOauth2DockerBuilder;
import org.etcsoft.dockertest.builders.Oauth2DockerBuilder;
import org.etcsoft.dockertest.builders.SpringAppDockerBuilder;

public class DockerContainerFactory {

    public static MysqlDBDockerBuilder getMysqlBuilder() {
        return new MysqlDBDockerBuilder();
    }

    public static MysqlOauth2DockerBuilder getMysqlOauth2Builder() {
        return new MysqlOauth2DockerBuilder();
    }

    public static SpringAppDockerBuilder getSpringAppBuilder() {
        return new SpringAppDockerBuilder();
    }

    public static Oauth2DockerBuilder getOauth2ServerBuilder() {
        return new Oauth2DockerBuilder();
    }
}
