package org.etcsoft.dockertest.docker;

public interface SpringDockerApp extends DockerContainer {
    String getExposedRandomPort(String port);
}
