package org.etcsoft.dockertest;

import lombok.Getter;

/**
 * This exception is thrown when an error happened.
 */
public class DockerIllegalException extends RuntimeException {
    @Getter
    private final DockerErrorCodes errorCode;

    /**
     * It throws the exception with a message
     * @param errorCode AppDirect error code
     * @param message Message added to the exception
     */
    public DockerIllegalException(DockerErrorCodes errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
    }

}
