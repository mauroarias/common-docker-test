package org.etcsoft.dockertest.docker;

import com.spotify.docker.client.DockerClient;
import com.spotify.docker.client.LogStream;
import com.spotify.docker.client.messages.ContainerConfig;
import com.spotify.docker.client.messages.ContainerCreation;
import com.spotify.docker.client.messages.Network;
import lombok.SneakyThrows;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SpringDefaultDockerAppTest {

    private final static String IMAGE_NAME = "image";
    private final static String IMAGE_TAG = "tag";
    private final static String NETWORK_NAME = "network";
    private final static String CONTAINER_ID = "container";
    private final static String HOSTNAME = "hostname";
    private final static Set<String> PUBLISHED_PORTS = new HashSet<String>() {{
        add("9595");
    }};
    private final static int WAITING_CONTAINER_LOOP_TIME = 1;

    private final DockerClient dockerClient = mock(DockerClient.class);
    private final ContainerCreation containerCreation = mock(ContainerCreation.class);
    private final Network network = mock(Network.class);
    private final LogStream logStream = mock(LogStream.class);

    @Rule
    public final ExpectedException thrownExpected = ExpectedException.none();

    @Test
    public void whenInstanceMysqlOK_thenInstanceOk() {
        whenMockDefault();
        SpringDefaultDockerApp docker = getInstanceArgOk();
        Assert.assertEquals(CONTAINER_ID, docker.getContainerId());
    }

    @Test
    public void whenGetRandomPort_thenPortOk() {
        whenMockDefault();
        SpringDefaultDockerApp docker = getInstanceArgOk();
        Assert.assertEquals(CONTAINER_ID, docker.getContainerId());
        Assert.assertEquals(PUBLISHED_PORTS, docker.getExposedPorts());
    }

    @Test
    @SneakyThrows
    public void whenInstanceClose_ThenInstanceOk() {
        whenMockDefault();
        SpringDefaultDockerApp docker = getInstanceArgOk();
        Assert.assertEquals(CONTAINER_ID, docker.getContainerId());
        Assert.assertEquals(false, docker.isClosed());
        docker.close();
        Assert.assertEquals(true, docker.isClosed());
    }

    @SneakyThrows
    private void whenMockDefault() {
        when(dockerClient.createContainer(any(ContainerConfig.class))).thenReturn(containerCreation);
        when(containerCreation.id()).thenReturn(CONTAINER_ID);
        when(dockerClient.listNetworks()).thenReturn(Arrays.asList(network));
        when(network.name()).thenReturn(NETWORK_NAME);
        when(dockerClient.logs(eq(CONTAINER_ID),
                any(DockerClient.LogsParam.class),
                any(DockerClient.LogsParam.class))).thenReturn(logStream);
        when(logStream.readFully()).thenReturn(DockerConstants.DOCKER_SPRING_WAITING_START_APP_MESSAGE);
    }

    private SpringDefaultDockerApp getInstanceArgOk() {
        return new SpringDefaultDockerApp(IMAGE_NAME,
                IMAGE_TAG,
                true,
                PUBLISHED_PORTS,
                null,
                dockerClient,
                WAITING_CONTAINER_LOOP_TIME,
                HOSTNAME,
                null,
                null);
    }
}
