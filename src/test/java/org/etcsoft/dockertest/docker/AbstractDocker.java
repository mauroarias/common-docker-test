package org.etcsoft.dockertest.docker;

import com.spotify.docker.client.DockerClient;

import java.util.Map;
import java.util.Set;

public class AbstractDocker extends DockerAbstractContainer {
    AbstractDocker(String imageName,
                   String imageTag,
                   boolean autoPull,
                   Set<String> envs,
                   int dockerWaitingLoop,
                   String waitingMessage,
                   DockerClient dockerClient,
                   Set<String> exposedPorts,
                   String hostname,
                   Set<String> links,
                   Map<String, DockerContainer> parentContainers) {
        super(imageName,
                imageTag,
                autoPull,
                envs,
                dockerWaitingLoop,
                waitingMessage,
                dockerClient,
                exposedPorts,
                hostname,
                links,
                parentContainers);
    }
}
