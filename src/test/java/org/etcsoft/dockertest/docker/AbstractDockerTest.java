package org.etcsoft.dockertest.docker;

import com.spotify.docker.client.DockerClient;
import com.spotify.docker.client.LogStream;
import com.spotify.docker.client.exceptions.DockerException;
import com.spotify.docker.client.messages.ContainerConfig;
import com.spotify.docker.client.messages.ContainerCreation;
import lombok.SneakyThrows;
import org.etcsoft.dockertest.DockerErrorCodes;
import org.etcsoft.dockertest.DockerIllegalException;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.HashSet;
import java.util.Set;

import static java.lang.String.format;
import static org.etcsoft.dockertest.docker.DockerConstants.DOCKER_SPRING_STARTING_FAIL;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AbstractDockerTest {

    private final static String IMAGE_NAME = "image";
    private final static String IMAGE_TAG = "tag";
    private final static String CONTAINER_ID = "container";
    private final static String HOSTNAME = "hostname";
    private final static String IMAGE_FULL_NAME = format("%s:%s", IMAGE_NAME, IMAGE_TAG);
    private final static String IMAGE_FULL_NAME_LATEST_TAG = format("%s:latest", IMAGE_NAME);
    private final static String WAITING_STARTING_CONTAINER_MESSAGE = "It is ready!!!";
    private final static Set<String> PORT = new HashSet<String>() {{
        add("3333");
    }};
    private final static Set<String> PORTS = new HashSet<String>() {{
        add("3333");
        add("6666");
        add("4444");
    }};
    private final static Set<String> ENVS = new HashSet<String>() {{
        add("A");
        add("B");
    }};
    private final static int WAITING_CONTAINER_LOOP_TIME = 1;

    private final DockerClient dockerClient = mock(DockerClient.class);
    private final ContainerCreation containerCreation = mock(ContainerCreation.class);
    private final LogStream logStream = mock(LogStream.class);

    @Rule
    public final ExpectedException thrownExpected = ExpectedException.none();

    @Test
    public void whenInstanceAllArgOK_thenInstanceOk() {
        whenMockDefault(WAITING_STARTING_CONTAINER_MESSAGE, true);
        AbstractDocker docker = getInstanceArgOk(true, IMAGE_NAME, IMAGE_TAG, HOSTNAME, PORT);
        Assert.assertEquals(CONTAINER_ID, docker.getContainerId());
        Assert.assertEquals(IMAGE_FULL_NAME, docker.getImageFullName());
    }

    @Test
    public void whenInstanceExposedPortsEmpty_thenOk() {
        whenMockDefault(WAITING_STARTING_CONTAINER_MESSAGE, true);
        AbstractDocker docker = getInstanceArgOk(true, IMAGE_NAME, IMAGE_TAG, HOSTNAME, new HashSet<>());
        Assert.assertEquals(CONTAINER_ID, docker.getContainerId());
    }

    @Test
    public void whenInstanceHostnameEmpty_thenOk() {
        whenMockDefault(WAITING_STARTING_CONTAINER_MESSAGE, true);
        AbstractDocker docker = getInstanceArgOk(true, IMAGE_NAME, IMAGE_TAG, "", new HashSet<>());
        Assert.assertEquals(CONTAINER_ID, docker.getContainerId());
    }

    @Test
    public void whenInstanceHostnameNull_thenOk() {
        whenMockDefault(WAITING_STARTING_CONTAINER_MESSAGE, true);
        AbstractDocker docker = getInstanceArgOk(true, IMAGE_NAME, IMAGE_TAG, null, new HashSet<>());
        Assert.assertEquals(CONTAINER_ID, docker.getContainerId());
    }

    @Test
    public void whenInstanceExposedPortsNull_thenOk() {
        whenMockDefault(WAITING_STARTING_CONTAINER_MESSAGE, true);
        AbstractDocker docker = getInstanceArgOk(true, IMAGE_NAME, IMAGE_TAG, HOSTNAME, null);
        Assert.assertEquals(CONTAINER_ID, docker.getContainerId());
    }

    @Test
    public void whenInstanceExposedPorts_thenOk() {
        whenMockDefault(WAITING_STARTING_CONTAINER_MESSAGE, true);
        AbstractDocker docker = getInstanceArgOk(true, IMAGE_NAME, IMAGE_TAG, HOSTNAME, PORTS);
        Assert.assertEquals(CONTAINER_ID, docker.getContainerId());
    }

    @Test
    public void whenInstanceTagBlank_thenInstanceOk() {
        whenMockDefault(WAITING_STARTING_CONTAINER_MESSAGE, true);
        AbstractDocker docker = getInstanceArgOk(true, IMAGE_NAME, "", HOSTNAME, PORT);
        Assert.assertEquals(CONTAINER_ID, docker.getContainerId());
        Assert.assertEquals(IMAGE_FULL_NAME_LATEST_TAG, docker.getImageFullName());
        docker = getInstanceArgOk(true, IMAGE_NAME, null, HOSTNAME, PORT);
        Assert.assertEquals(CONTAINER_ID, docker.getContainerId());
        Assert.assertEquals(IMAGE_FULL_NAME_LATEST_TAG, docker.getImageFullName());
    }

    @Test
    public void whenInstanceEnvNullEmpty_thenInstanceOk() {
        whenMockDefault(WAITING_STARTING_CONTAINER_MESSAGE, true);
        AbstractDocker docker = new AbstractDocker(IMAGE_NAME,
                IMAGE_TAG,
                true,
                new HashSet<>(),
                WAITING_CONTAINER_LOOP_TIME,
                WAITING_STARTING_CONTAINER_MESSAGE,
                dockerClient,
                PORT,
                HOSTNAME,
                null,
                null);
        Assert.assertEquals(CONTAINER_ID, docker.getContainerId());
        Assert.assertEquals(IMAGE_FULL_NAME, docker.getImageFullName());
        docker = new AbstractDocker(IMAGE_NAME,
                IMAGE_TAG,
                true,
                null,
                WAITING_CONTAINER_LOOP_TIME,
                WAITING_STARTING_CONTAINER_MESSAGE,
                dockerClient,
                PORT,
                HOSTNAME,
                null,
                null);
        Assert.assertEquals(CONTAINER_ID, docker.getContainerId());
        Assert.assertEquals(IMAGE_FULL_NAME, docker.getImageFullName());
    }

    @Test
    public void whenInstanceAllArgOKNotAutoPull_thenInstanceOk() {
        whenMockDefault(WAITING_STARTING_CONTAINER_MESSAGE, false);
        AbstractDocker docker = getInstanceArgOk(false, IMAGE_NAME, IMAGE_TAG, HOSTNAME, PORT);
        Assert.assertEquals(CONTAINER_ID, docker.getContainerId());
    }

    @Test
    @SneakyThrows
    public void whenInstanceClose_ThenInstanceOk() {
        whenMockDefault(WAITING_STARTING_CONTAINER_MESSAGE, false);
        AbstractDocker docker = getInstanceArgOk(true, IMAGE_NAME, IMAGE_TAG, HOSTNAME, PORT);
        Assert.assertEquals(CONTAINER_ID, docker.getContainerId());
        Assert.assertEquals(false, docker.isClosed());
        docker.close();
        Assert.assertEquals(true, docker.isClosed());
    }

    @Test
    public void whenInstanceAllArgContainerNotUp_thenException() {
        whenMockDefault("", false);
        thrownExpected.expect(DockerIllegalException.class);
        thrownExpected.expectMessage("Timeout expired during starting container!!!");
        AbstractDocker docker = getInstanceArgOk(true, IMAGE_NAME, IMAGE_TAG, HOSTNAME, PORT);
    }

    @Test
    @SneakyThrows
    public void whenInstanceCreationContainerClientException_thenException() {
        when(dockerClient.createContainer(any(ContainerConfig.class))).thenThrow(DockerException.class);
        thrownExpected.expect(DockerIllegalException.class);
        validErrorCodeOnException(DockerErrorCodes.DOCKER_CLIENT_ERROR);
    }

    @Test
    @SneakyThrows
    public void whenInstanceCreationContainerInterruptedException_thenException() {
        when(dockerClient.createContainer(any(ContainerConfig.class))).thenThrow(InterruptedException.class);
        thrownExpected.expect(DockerIllegalException.class);
        validErrorCodeOnException(DockerErrorCodes.DOCKER_INTERRUPTED);
    }

    @Test
    @SneakyThrows
    public void whenInstanceCreationContainerException_thenException() {
        when(dockerClient.createContainer(any(ContainerConfig.class))).thenThrow(Exception.class);
        thrownExpected.expect(DockerIllegalException.class);
        validErrorCodeOnException(DockerErrorCodes.UNKNOWN_ERROR);
    }

    @Test
    @SneakyThrows
    public void whenInstanceContainerIdClientException_thenException() {
        when(dockerClient.createContainer(any(ContainerConfig.class))).thenReturn(containerCreation);
        when(containerCreation.id()).thenThrow(DockerException.class);
        thrownExpected.expect(DockerIllegalException.class);
        validErrorCodeOnException(DockerErrorCodes.DOCKER_CLIENT_ERROR);
    }

    @Test
    @SneakyThrows
    public void whenInstanceContainerIdInterruptedException_thenException() {
        when(dockerClient.createContainer(any(ContainerConfig.class))).thenReturn(containerCreation);
        when(containerCreation.id()).thenThrow(InterruptedException.class);
        thrownExpected.expect(DockerIllegalException.class);
        validErrorCodeOnException(DockerErrorCodes.DOCKER_INTERRUPTED);
    }

    @Test
    @SneakyThrows
    public void whenInstanceContainerIdException_thenException() {
        when(dockerClient.createContainer(any(ContainerConfig.class))).thenReturn(containerCreation);
        when(containerCreation.id()).thenThrow(Exception.class);
        thrownExpected.expect(DockerIllegalException.class);
        validErrorCodeOnException(DockerErrorCodes.UNKNOWN_ERROR);
    }

    @Test
    @SneakyThrows
    public void whenInstanceGetLogsClientException_thenException() {
        when(dockerClient.createContainer(any(ContainerConfig.class))).thenReturn(containerCreation);
        when(containerCreation.id()).thenReturn(CONTAINER_ID);
        when(dockerClient.logs(eq(CONTAINER_ID),
                any(DockerClient.LogsParam.class),
                any(DockerClient.LogsParam.class))).thenThrow(DockerException.class);
        thrownExpected.expect(DockerIllegalException.class);
        validErrorCodeOnException(DockerErrorCodes.DOCKER_CLIENT_ERROR);
    }

    @Test
    @SneakyThrows
    public void whenInstanceGetLogsInterruptedException_thenException() {
        when(dockerClient.createContainer(any(ContainerConfig.class))).thenReturn(containerCreation);
        when(containerCreation.id()).thenReturn(CONTAINER_ID);
        when(dockerClient.logs(eq(CONTAINER_ID),
                any(DockerClient.LogsParam.class),
                any(DockerClient.LogsParam.class))).thenThrow(InterruptedException.class);
        thrownExpected.expect(DockerIllegalException.class);
        validErrorCodeOnException(DockerErrorCodes.DOCKER_INTERRUPTED);
    }

    @Test
    @SneakyThrows
    public void whenInstanceGetLogsException_thenException() {
        when(dockerClient.createContainer(any(ContainerConfig.class))).thenReturn(containerCreation);
        when(containerCreation.id()).thenReturn(CONTAINER_ID);
        when(dockerClient.logs(eq(CONTAINER_ID),
                any(DockerClient.LogsParam.class),
                any(DockerClient.LogsParam.class))).thenThrow(Exception.class);
        thrownExpected.expect(DockerIllegalException.class);
        validErrorCodeOnException(DockerErrorCodes.UNKNOWN_ERROR);
    }

    @Test
    @SneakyThrows
    public void whenInstanceReadLogsClientException_thenException() {
        when(dockerClient.createContainer(any(ContainerConfig.class))).thenReturn(containerCreation);
        when(containerCreation.id()).thenReturn(CONTAINER_ID);
        when(dockerClient.logs(eq(CONTAINER_ID),
                any(DockerClient.LogsParam.class),
                any(DockerClient.LogsParam.class))).thenReturn(logStream);
        when(logStream.readFully()).thenThrow(DockerException.class);
        thrownExpected.expect(DockerIllegalException.class);
        validErrorCodeOnException(DockerErrorCodes.DOCKER_CLIENT_ERROR);
    }

    @Test
    @SneakyThrows
    public void whenInstanceReadLogsInterruptedException_thenException() {
        when(dockerClient.createContainer(any(ContainerConfig.class))).thenReturn(containerCreation);
        when(containerCreation.id()).thenReturn(CONTAINER_ID);
        when(dockerClient.logs(eq(CONTAINER_ID),
                any(DockerClient.LogsParam.class),
                any(DockerClient.LogsParam.class))).thenReturn(logStream);
        when(logStream.readFully()).thenThrow(InterruptedException.class);
        thrownExpected.expect(DockerIllegalException.class);
        validErrorCodeOnException(DockerErrorCodes.DOCKER_INTERRUPTED);
    }

    @Test
    @SneakyThrows
    public void whenInstanceReadLogsException_thenException() {
        when(dockerClient.createContainer(any(ContainerConfig.class))).thenReturn(containerCreation);
        when(containerCreation.id()).thenReturn(CONTAINER_ID);
        when(dockerClient.logs(eq(CONTAINER_ID),
                any(DockerClient.LogsParam.class),
                any(DockerClient.LogsParam.class))).thenReturn(logStream);
        when(logStream.readFully()).thenThrow(Exception.class);
        thrownExpected.expect(DockerIllegalException.class);
        validErrorCodeOnException(DockerErrorCodes.UNKNOWN_ERROR);
    }

    @Test
    @SneakyThrows
    public void whenInstanceStartingFail_thenException() {
        whenMockDefault(DOCKER_SPRING_STARTING_FAIL, false);
        thrownExpected.expect(DockerIllegalException.class);
        getInstanceArgOk(true, IMAGE_NAME, IMAGE_TAG, HOSTNAME, PORT);
    }

    @Test
    public void whenInstanceImageNameBlank_thenException() {
        thrownExpected.expect(DockerIllegalException.class);
        thrownExpected.expectMessage("Image name cannot be empty or null");
        getInstanceArgOk(false, "", IMAGE_TAG, HOSTNAME, PORT);
        getInstanceArgOk(false, null, IMAGE_TAG, HOSTNAME, PORT);
    }

    @Test
    public void whenInstanceWaitingMessageBlank_thenException() {
        thrownExpected.expect(DockerIllegalException.class);
        thrownExpected.expectMessage("Waiting message cannot be empty or null");
        new AbstractDocker(IMAGE_NAME,
                IMAGE_TAG,
                true,
                ENVS,
                WAITING_CONTAINER_LOOP_TIME,
                "",
                dockerClient,
                PORT,
                HOSTNAME,
                null,
                null);
        new AbstractDocker(IMAGE_NAME,
                IMAGE_TAG,
                true,
                ENVS,
                WAITING_CONTAINER_LOOP_TIME,
                null,
                dockerClient,
                PORT,
                HOSTNAME,
                null,
                null);
    }

    @Test
    public void whenInstanceDockerClientNull_thenException() {
        thrownExpected.expect(DockerIllegalException.class);
        thrownExpected.expectMessage("Docker client cannot be null");
        new AbstractDocker(IMAGE_NAME,
                IMAGE_TAG,
                true,
                ENVS,
                WAITING_CONTAINER_LOOP_TIME,
                WAITING_STARTING_CONTAINER_MESSAGE,
                null,
                PORT,
                HOSTNAME,
                null,
                null);
    }

    private void validErrorCodeOnException(DockerErrorCodes errorCode) {
        try {
            new AbstractDocker(IMAGE_NAME,
                    IMAGE_TAG,
                    true,
                    ENVS,
                    WAITING_CONTAINER_LOOP_TIME,
                    WAITING_STARTING_CONTAINER_MESSAGE,
                    dockerClient,
                    PORT,
                    HOSTNAME,
                    null,
                    null);
            Assert.fail("An exception was expected...");
        } catch (DockerIllegalException ex) {
            Assert.assertEquals(errorCode, ex.getErrorCode());
            throw ex;
        } catch (Exception ex) {
            Assert.fail("An Docker illegal exception was expected...");
        }
    }

    @SneakyThrows
    private void whenMockDefault(String logMessage, boolean mockListNetworks) {
        when(dockerClient.createContainer(any(ContainerConfig.class))).thenReturn(containerCreation);
        when(containerCreation.id()).thenReturn(CONTAINER_ID);
        when(dockerClient.logs(eq(CONTAINER_ID),
                any(DockerClient.LogsParam.class),
                any(DockerClient.LogsParam.class))).thenReturn(logStream);
        when(logStream.readFully()).thenReturn(logMessage);
    }

    private AbstractDocker getInstanceArgOk(boolean autoPull,
                                            String imageName,
                                            String imageTag,
                                            String hostname,
                                            Set<String> port) {
        return new AbstractDocker(imageName,
                imageTag,
                autoPull,
                ENVS,
                WAITING_CONTAINER_LOOP_TIME,
                WAITING_STARTING_CONTAINER_MESSAGE,
                dockerClient,
                port,
                hostname,
                null,
                null
        );
    }
}
