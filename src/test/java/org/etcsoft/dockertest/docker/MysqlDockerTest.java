package org.etcsoft.dockertest.docker;

import com.spotify.docker.client.DockerClient;
import com.spotify.docker.client.LogStream;
import com.spotify.docker.client.messages.ContainerConfig;
import com.spotify.docker.client.messages.ContainerCreation;
import com.spotify.docker.client.messages.Network;
import com.zaxxer.hikari.HikariDataSource;
import lombok.SneakyThrows;
import org.etcsoft.dockertest.DockerIllegalException;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Arrays;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MysqlDockerTest {

    private final static String IMAGE_NAME = "image";
    private final static String IMAGE_TAG = "tag";
    private final static String NETWORK_NAME = "network";
    private final static String CONTAINER_ID = "container";
    private final static String HOSTNAME = "hostname";
    private final static int WAITING_CONTAINER_LOOP_TIME = 1;

    private final DockerClient dockerClient = mock(DockerClient.class);
    private final HikariDataSource hikariDataSource = mock(HikariDataSource.class);
    private final ContainerCreation containerCreation = mock(ContainerCreation.class);
    private final Network network = mock(Network.class);
    private final LogStream logStream = mock(LogStream.class);
    private final Connection connection = mock(Connection.class);
    private final PreparedStatement preparedStatement = mock(PreparedStatement.class);

    @Rule
    public final ExpectedException thrownExpected = ExpectedException.none();

    @Test
    public void whenInstanceAllArgOK_thenInstanceOk() {
        whenMockDefault();
        MysqlDocker docker = getInstanceArgOk();
        Assert.assertEquals(CONTAINER_ID, docker.getContainerId());
    }

    @Test
    @SneakyThrows
    public void whenInstanceClose_ThenInstanceOk() {
        whenMockDefault();
        MysqlDocker docker = getInstanceArgOk();
        Assert.assertEquals(CONTAINER_ID, docker.getContainerId());
        Assert.assertEquals(false, docker.isClosed());
        docker.close();
        Assert.assertEquals(true, docker.isClosed());
    }

    @Test
    public void whenGetDatasource_thenDatasourceOk() {
        whenMockDefault();
        MysqlDocker docker = getInstanceArgOk();
        Assert.assertEquals(CONTAINER_ID, docker.getContainerId());
        Assert.assertEquals(hikariDataSource, docker.getDatasource());
    }

    @Test
    @SneakyThrows
    public void whenLoadSchema_thenOk() {
        whenMockDefault();
        when(hikariDataSource.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(any(String.class))).thenReturn(preparedStatement);
        MysqlDocker docker = getInstanceArgOk();
        Assert.assertEquals(CONTAINER_ID, docker.getContainerId());
        docker.loadSchema(Arrays.asList("a", "b", "c"));
    }

    @Test
    @SneakyThrows
    public void whenLoadSchemaPreparedStatementException_ThenException() {
        whenMockDefault();
        thrownExpected.expect(DockerIllegalException.class);
        thrownExpected.expectMessage("Error loading schema, exception: ");
        when(hikariDataSource.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(any(String.class))).thenThrow(Exception.class);
        MysqlDocker docker = getInstanceArgOk();
        Assert.assertEquals(CONTAINER_ID, docker.getContainerId());
        docker.loadSchema(Arrays.asList("a", "b", "c"));
    }

    @Test
    @SneakyThrows
    public void whenLoadSchemaGetConnectionException_ThenException() {
        whenMockDefault();
        thrownExpected.expect(DockerIllegalException.class);
        thrownExpected.expectMessage("Error loading schema, exception: ");
        when(hikariDataSource.getConnection()).thenThrow(Exception.class);
        MysqlDocker docker = getInstanceArgOk();
        Assert.assertEquals(CONTAINER_ID, docker.getContainerId());
        docker.loadSchema(Arrays.asList("a", "b", "c"));
    }

    @SneakyThrows
    private void whenMockDefault() {
        when(dockerClient.createContainer(any(ContainerConfig.class))).thenReturn(containerCreation);
        when(containerCreation.id()).thenReturn(CONTAINER_ID);
        when(dockerClient.listNetworks()).thenReturn(Arrays.asList(network));
        when(network.name()).thenReturn(NETWORK_NAME);
        when(dockerClient.logs(eq(CONTAINER_ID),
                any(DockerClient.LogsParam.class),
                any(DockerClient.LogsParam.class))).thenReturn(logStream);
        when(logStream.readFully()).thenReturn(DockerConstants.DOCKER_MYSQL_WAITING_START_MESSAGE);
    }

    private MysqlDocker getInstanceArgOk() {
        return new MysqlDefaultDocker(IMAGE_NAME,
                IMAGE_TAG,
                true,
                dockerClient,
                hikariDataSource,
                WAITING_CONTAINER_LOOP_TIME,
                HOSTNAME,
                null,
                null);
    }
}
