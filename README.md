# DockerTest
This library allows to handle easily test containers used inside of integration tests.
It's implemented in java and microservices spring boot. Using: docker clients, access to Mysql DB using JDBC connector & Hikari Datasource, 
Exception Handling, Beans, Lombok, Spotify Docker client, immutable objects, Junit, Mock classes, config and more.

## Requirements
* java 8
* Maven 3
* spotify - docker-maven-plugin
 
## COMPONENTS SUPPORTED
* Mysql.
* Oauth2 Mysql with schema pre-loaded.
* Spring boot Apps.
* Oauthserver with implicit mysql support

## WHAT NEXT

* Improve readme file. 
* Improve unit tests.
* Add support to Cassandra, MongoDB, Elasticserach and others components.
* Improve logs/traces.
* Create a first public maven library.

## TESTS

### Unit test
This project includes unit tests of the some business class, using Junit and mockito frameworks.
please see ../src/test/java/org/etcsoft/dockertest.docker to find more details

## COMPILING AND RUNNING TESTS

### To compile and run unit tests:

```
mvn clean package
```

## HOW TO USE

### Mysql instances:

use **MysqlDBDockerBuilder** to build a **MysqlDocker** instance as:

```
DockerContainerFactory.getMysqlBuilder()
        .mysqlImageName(mysqlImageName)         //mandatory image name. Example: "mauroarias/mysql-oraclelinux7"
        .mysqlImageTag(mysqlImageTag)           //optional tag, by default "latest". Example: "5.6.34"
        .hostname(hostname)                     //optional hostname, by default empty. Example "myhostname"
        .autoPull(true)                         //optional flag to auto pull images, by default false
        .addContainerLink(containerId, alias)   //optional containers links, by default empty. These are container 
                                                //  will be linked to this container
        .addParentContainer(container)          //optional containers linked to be closed when the container is closed
        .build()                                //create and start the container Mysql.
```
 
### MysqlOauth2 instances:

use **MysqlOauth2DockerBuilder** to build a **MysqlDocker** instance with a oauth2 schema pre-loaded as:

```
DockerContainerFactory.getMysqlOauth2Builder()
        .imageName(imageName)                   //mandatory image name. Example: "mauroarias/mysql-oraclelinux7"
        .imageTag(imageTag)                     //optional tag, by default "latest". Example: "5.6.34"
        .hostname(hostname)                     //optional hostname, by default empty. Example "myhostname"
        .autoPull(true)                         //optional flag to auto pull images, by default false
        .addContainerLink(containerId, alias)   //optional containers links, by default empty. These are container 
                                                //  will be linked to this container
        .addParentContainer(container)          //optional containers linked to be closed when the container is closed
        .build()                                //create and start the container Mysql.
```

### SpringDockerApp instances:

use **MysqlOauth2DockerBuilder** to build a **SpringDockerApp** instance with spring boot microservice as:

```
DockerContainerFactory.getSpringAppBuilder()
        .imageName(imageName)                   //mandatory image name. Example: "mauroarias/boot-app"
        .imageTag(imageTag)                     //optional tag, by default "latest". Example: "1.0.0"
        .hostname(hostname)                     //optional hostname, by default empty. Example "myhostname"
        .autoPull(true)                         //optional flag to auto pull images, by default false
        .addContainerLink(containerId, alias)   //optional containers links, by default empty. These are container 
                                                //  will be linked to this container
        .addParentContainer(container)          //optional containers linked to be closed when the container is closed
        .addExposedPort(port)                   //optional exposed ports.
        .addEnvironmentVar(var)                 //optional environment variables.
        .build()                                //create and start the container.
```

### oauth2 server instances:

use **Oauth2DockerBuilder** to build a specific **SpringDockerApp** spring boot microservice with oauth2 support as:

`Note that this instance starts two containers: one Mysql and one SpringApp`

```
DockerContainerFactory.getSpringAppBuilder()
        .mysqlImageName(mysqlImageName)         //mandatory mysql image name. Example: "mauroarias/mysql-oraclelinux7"
        .mysqlImageTag(mysqlImageTag)           //optional mysql tag, by default "latest". Example: "5.6.34"
        .hostname(hostname)                     //optional hostname, by default empty. Example "myhostname"
        .oauth2ServerImageName(imageName)       //mandatory image app name. Example "mauroarias/authserver
        .autoPull(true)                         //optional flag to auto pull images, by default false
        .build()                                //create and start the container.
```
